#Google Chrome 116.0.5845.140
# 1.open web browser
# 2. open url
# 3.Enter username
# 4.Enter password
# 5.click on Login
# 6.Capture title of the home page
# 7. verify title of the page: OrangeHRM
# 8. Close browser
# (Webdriver is a module which is available in selenium package)
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
import time

service_obj = Service("/home/pravin/Documents/chromedriver-linux64/chromedriver")
driver = webdriver.Chrome(service=service_obj)
driver.get("https://opensource-demo.orangehrmlive.com")
driver.maximize_window()
time.sleep(4)