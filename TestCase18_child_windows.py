from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time
# Chrome driver - Chrome Browser

# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj)

# Navigate to the website
driver.get("https://the-internet.herokuapp.com/windows")
time.sleep(1)
driver.find_element(By.LINK_TEXT, "Click Here").click()

windowsOpened = driver.window_handles
time.sleep(1)

driver.switch_to.window(windowsOpened[1])
print(driver.find_element(By.TAG_NAME, "h3").text)
driver.switch_to.window(windowsOpened[0])
assert "Opening a new window" == driver.find_element(By.TAG_NAME,"h3").text


time.sleep(3)

# Close the WebDriver
driver.quit()