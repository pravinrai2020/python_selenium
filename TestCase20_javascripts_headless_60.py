#selenium does not have direct to scroll screen but using js we can do
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time
# Chrome driver - Chrome Browser
#for headless(UI not display)
chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument("headless")
chrome_options.add_argument("--start-maximized")
#to ignore certificate errors
chrome_options.add_argument("--ignore-certificate-errors")
# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj, options=chrome_options)

driver.get("https://rahulshettyacademy.com/AutomationPractice/")
driver.maximize_window()
time.sleep(1)

driver.execute_script("window.scrollBy(0,document.body.scrollHeight);")
time.sleep(1)
driver.get_screenshot_as_file("screen2.png")

time.sleep(3)

# Close the WebDriver
driver.quit()