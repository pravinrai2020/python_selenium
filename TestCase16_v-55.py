from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome(service=Service())
driver.implicitly_wait(2)

expectedList = ['Cucumber - 1 Kg', 'Raspberry - 1/4 Kg', 'Strawberry - 1/4 Kg']
actualList = []
# Navigate to the website
driver.get("https://rahulshettyacademy.com/seleniumPractise/")

driver.maximize_window()

driver.find_element(By.CSS_SELECTOR,".search-keyword").send_keys("ber")
time.sleep(1)

results = driver.find_elements(By.XPATH,"//div[@class='products']/div")
#in the above line of code sleep() must be placed because selenium does not wait and returns emmidiately empty list 'list[]'
# and it is still valid that means driver.implicitly_wait(5) does not work in this case
count = len(results)
assert count > 0
for result in results:
    time.sleep(0.5)
    actualList.append(result.find_element(By.XPATH,"h4").text)
    result.find_element(By.XPATH, "div/button").click()

assert expectedList == actualList

driver.find_element(By.CSS_SELECTOR,"img[alt='Cart']").click()
time.sleep(0.5)
driver.find_element(By.XPATH,"//button[text()='PROCEED TO CHECKOUT']").click()
time.sleep(1)
driver.find_element(By.CSS_SELECTOR,".promoCode").send_keys("rahulshettyacademy")

driver.find_element(By.CSS_SELECTOR,".promoBtn").click()

wait = WebDriverWait(driver,10)
wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR,".promoInfo")))

prices = driver.find_elements(By.CSS_SELECTOR,"tr td:nth-child(5) p")
sum = 0
for price in prices:
    sum = sum+int(price.text)

print(sum)
totalAmount = int(driver.find_element(By.CSS_SELECTOR, ".totAmt").text)

assert sum == totalAmount

discounted_amt = float(driver.find_element(By.CSS_SELECTOR,".discountAmt").text)
print(discounted_amt)

assert totalAmount > discounted_amt
# print(driver.find_element(By.CLASS_NAME, ".promoInfo").text)

time.sleep(4)
driver.quit()