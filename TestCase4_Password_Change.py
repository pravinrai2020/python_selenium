from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time
# Chrome driver - Chrome Browser

# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj)

# Navigate to the website
driver.get("https://rahulshettyacademy.com/client/")
# driver.get("https://rahulshettyacademy.com/client/auth/login")
# Wait for a moment
time.sleep(1)

driver.find_element(By.LINK_TEXT,"Forgot password?").click()
time.sleep(1)

driver.find_element(By.XPATH,"//form/div[1]/input").send_keys("rai.pravin@gmail.com")
time.sleep(1)

driver.find_element(By.CSS_SELECTOR,"form div:nth-child(2) input").send_keys("Asdf@123")
time.sleep(1)

driver.find_element(By.CSS_SELECTOR,"#confirmPassword").send_keys("Asdf@123")

# driver.find_element(By.XPATH,"//button[@type='submit']").click()
driver.find_element(By.XPATH,"//button[text()='Save New Password']").click()

# Wait for a moment

time.sleep(3)

# Close the WebDriver
driver.quit()