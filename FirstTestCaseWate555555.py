#Google Chrome 116.0.5845.140
# 1.open web browser
# 2. open url
# 3.Enter username
# 4.Enter password
# 5.click on Login
# 6.Capture title of the home page
# 7. verify title of the page: OrangeHRM
# 8. Close browser
# (Webdriver is a module which is available in selenium package)

from selenium import webdriver
from selenium.webdriver.chrome.service import Service

from selenium.webdriver.common.by import By
# Specify the path to the Chrome WebDriver executable
# webdriver_path = '/path/to/chromedriver'

# Initialize the WebDriver with the specified path
# driver = webdriver.Chrome(executable_path=webdriver_path)

# Rest of your script...

# from selenium.webdriver.chrome.service import Service


# chromedvr = "/home/pravin/Downloads/chromedriver_linux64/chromedriver.exe"

serv_obj =Service("/home/pravin/Downloads/chromedriver_linux64/chromedriver.exe")
driver = webdriver.Chrome(service=serv_obj)

driver.get("https://opensource-demo.orangehrmlive.com")
# driver.find_element_by_name("username").send_keys("Admin")
# driver.find_element_by_name("password").send_keys("admin123")
# driver.find_element_by_id("btnLogin").click()
#selenium4
driver.find_element(By.NAME,"username").send_keys("Admin")
driver.find_element(By.NAME,"password").send_keys("admin123")
driver.find_element(By.ID,"btnLogin").click()

act_title = driver.title
exp_title = "OrangeHRM"

if act_title == exp_title:
    print("Login test passed")
else:
    print("Login Test Failed")

driver.close()

