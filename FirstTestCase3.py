from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

# Chrome driver - Chrome Browser

# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj)

# Navigate to the website
driver.get("https://rahulshettyacademy.com/angularpractice/")

# Find and interact with elements on the webpage
driver.find_element(By.NAME, "email").send_keys("hello@gmail.com")
driver.find_element(By.ID, "exampleInputPassword1").send_keys("123456")
driver.find_element(By.ID, "exampleCheck1").click()

# Use proper XPath and CSS Selector syntax
driver.find_element(By.CSS_SELECTOR, "input[name='name']").send_keys("Pravin")
driver.find_element(By.XPATH, "//input[@type='submit']").click()

# Wait for a moment to ensure the success message appears
import time
time.sleep(2)

# Find the success message and print its text
message = driver.find_element(By.CLASS_NAME, "alert-success").text
print(message)

# Close the WebDriver
driver.quit()