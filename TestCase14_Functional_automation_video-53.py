# https://cloco.udemy.com/course/learn-selenium-automation-in-easy-python-language/learn/lecture/16963838#overview
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome(service=Service())
driver.implicitly_wait(2)

# Navigate to the website
driver.get("https://rahulshettyacademy.com/seleniumPractise/")

driver.maximize_window()

driver.find_element(By.CSS_SELECTOR,".search-keyword").send_keys("ber")
time.sleep(1)

results = driver.find_elements(By.XPATH,"//div[@class='products']/div")
#in the above line of code sleep() must be placed because selenium does not wait and returns emmidiately empty list 'list[]'
# and it is still valid that means driver.implicitly_wait(5) does not work in this case
count = len(results)
assert count>0
for result in results:
    time.sleep(0.5)
    result.find_element(By.XPATH, "div/button").click()

driver.find_element(By.CSS_SELECTOR,"img[alt='Cart']").click()
time.sleep(0.5)
driver.find_element(By.XPATH,"//button[text()='PROCEED TO CHECKOUT']").click()
time.sleep(1)
#sum validation
prices = driver.find_elements(By.CSS_SELECTOR,"tr td:nth-child(5) p")
sum = 0
for price in prices:
    sum = sum+int(price.text)

print(sum)
totalAmount = int(driver.find_element(By.CSS_SELECTOR, ".totAmt").text)

assert sum == totalAmount

time.sleep(4)
driver.quit()