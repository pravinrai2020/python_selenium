from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

# Chrome driver - Chrome Browser

# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj)

name = "Pravin Khaling"
# Navigate to the website
driver.get("https://rahulshettyacademy.com/seleniumPractise/")

import time
driver.maximize_window()

driver.find_element(By.CSS_SELECTOR,".search-keyword").send_keys("ber")
time.sleep(1)

results = driver.find_elements(By.XPATH,"//div[@class='products']/div")
count = len(results)
assert count>0

time.sleep(1)
driver.quit()