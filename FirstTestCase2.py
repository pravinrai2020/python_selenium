from selenium import webdriver
from selenium.webdriver.chrome.service import Service

#Chrome driver -Chrome Broser

service_obj = Service("/home/pravin/Documents/chromedriver-linux64/chromedriver")
driver = webdriver.Chrome(service=service_obj)

driver.maximize_window()
driver.get("https://rahulshettyacademy.com")

print(driver.title)
print(driver.current_url)
driver.get("https://courses.rahulshettyacademy.com")
# driver.minimize_window()
driver.back()
driver.get("https://rahulshettyacademy.com")
driver.back()
driver.refresh()
driver.forward()

# driver.close()