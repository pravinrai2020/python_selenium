from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(service=Service())

# Navigate to the website
driver.get("https://rahulshettyacademy.com/seleniumPractise/")

driver.maximize_window()

driver.find_element(By.CSS_SELECTOR,".search-keyword").send_keys("ber")
time.sleep(1)

results = driver.find_elements(By.XPATH,"//div[@class='products']/div")
count = len(results)
assert count>0
for result in results:
    time.sleep(0.5)
    result.find_element(By.XPATH, "div/button").click()

driver.find_element(By.CSS_SELECTOR,"img[alt='Cart']").click()
time.sleep(0.5)
driver.find_element(By.XPATH,"//button[text()='PROCEED TO CHECKOUT']").click()
time.sleep(1) #it takes time so we must put sleep() here

driver.find_element(By.CSS_SELECTOR,".promoCode").send_keys("Pravin Khaling")
time.sleep(1)
driver.find_element(By.CSS_SELECTOR,".promoBtn").click()
time.sleep(5)
print(driver.find_element(By.CLASS_NAME, ".promoInfo").text)

time.sleep(4)
driver.quit()