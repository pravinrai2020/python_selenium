from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

# Chrome driver - Chrome Browser

# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj)

name = "Pravin Khaling"
# Navigate to the website
driver.get("https://rahulshettyacademy.com/AutomationPractice/")

import time

driver.maximize_window()
time.sleep(1)
driver.find_element(By.CSS_SELECTOR,"#name").send_keys(name)
driver.find_element(By.ID, "alertbtn").click()

alert = driver.switch_to.alert
alertText = alert.text
# print(alertText)
time.sleep(1)
assert name in alertText #assertion check
alert.accept() #click on ok

# alert.dismiss()

time.sleep(1)
driver.quit()