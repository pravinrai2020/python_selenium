from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome(service=Service())
driver.implicitly_wait(2)

expectedList = ['Cucumber - 1 Kg', 'Raspberry - 1/4 Kg', 'Strawberry - 1/4 Kg']
actualList = []
# Navigate to the website
driver.get("https://rahulshettyacademy.com/AutomationPractice/")

driver.maximize_window()

action = ActionChains(driver)
# action.double_click(driver.find_element(By.CSS_SELECTOR))
# action.drag_and_drop()
action.move_to_element(driver.find_element(By.ID,"mousehover")).perform()
time.sleep(1)
# action.context_click(driver.find_element(By.LINK_TEXT,"Top")).perform() #right click
action.move_to_element(driver.find_element(By.LINK_TEXT,"Reload")).perform()
time.sleep(4)
driver.quit()