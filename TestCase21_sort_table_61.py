#selenium does not have direct to scroll screen but using js we can do
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time
# Chrome driver - Chrome Browser
# Specify the path to your Chrome WebDriver executable
webdriver_path = "/home/pravin/Documents/chromedriver-linux64/chromedriver"

# Create a service object using the specified path
service_obj = Service(executable_path=webdriver_path)

# Initialize the Chrome WebDriver with the service object
driver = webdriver.Chrome(service=service_obj)
browserSortedVeggies = []
driver.get("https://rahulshettyacademy.com/seleniumPractise/#/offers")
driver.maximize_window()
time.sleep(1)
driver.find_element(By.XPATH,"//span[text()='Veg/fruit name']").click()
#collect all veggie names -> BrowserSortedveggieList
veggieWebElements = driver.find_elements(By.XPATH,"//tr/td[1]")
for ele in veggieWebElements:
    browserSortedVeggies.append(ele.text)

originalBrowserSortedList = browserSortedVeggies.copy()

browserSortedVeggies.sort()

assert browserSortedVeggies == originalBrowserSortedList




    time.sleep(5)

# Close the WebDriver
driver.quit()